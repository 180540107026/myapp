package com.example.myapp;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapp.adapter.UserListAdapter;
import com.example.myapp.util.Constant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SecondActivity extends AppCompatActivity {

    ListView lvUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViewReference();
        bindValuesID();
    }


    private void bindValuesID() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));

        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);
        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long itemID) {
                Toast.makeText(SecondActivity.this, userList.get(position).get(Constant.GENDER).toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    void initViewReference() {
        lvUsers = findViewById(R.id.lvActUserList);
    }


}


